import patients.Patient;
import patients.PatientRegistry;

import java.io.*;

public class HostpitalCSV {

    /**
     * Method that writes data in the registry to a csv file
     * @param fileName the file to write the data to
     * @param patientRegistry the registry to read data from
     */
    public static void writeToCsvFile(File fileName, PatientRegistry patientRegistry){
        try (FileWriter writer = new FileWriter(fileName)){
            writer.write("firstName;lastName;generalPractitioner;socialSecurityNumber\n");
            for (Patient p : patientRegistry.getPatients()) {
                writer.write(p.getFirstName() + ";" + p.getLastName() + ";" + p.getGeneralPractitioner() + ";" + p.getSocialSecurityNumber() + ";\n");
            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method to read data from a CSV file and add it to the table
     * @param fileName name of the CSV file
     * @param patientRegistry the registry to put the read data into
     * @return
     */
    public static PatientRegistry readFromCsvFile(File fileName, PatientRegistry patientRegistry){
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))){

            patientRegistry.getPatients().clear();

            String line = "";
            reader.readLine();
            while((line = reader.readLine()) != null){
                String[] patientsArray = line.split(";");
                patientRegistry.addPatient(new Patient(patientsArray[3], patientsArray[0], patientsArray[1], "", patientsArray[2]));
            }
            return patientRegistry;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
