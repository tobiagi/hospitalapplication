package patients;

import java.util.Objects;

public class Patient {

    private String firstName;
    private String lastName;
    private String socialSecurityNumber;
    private String diagnosis;
    private String generalPractitioner;

    /**
     * Constructor used when editing a patient that already exists in the register
     * @param firstName first name of patient
     * @param lastName last name of patient
     * @param socialSecurityNumber patient's social security number
     * @param diagnosis patient's diagnosis
     * @param generalPractitioner patients general practitioner
     */
    public Patient(String firstName, String lastName, String socialSecurityNumber, String diagnosis, String generalPractitioner) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.socialSecurityNumber = socialSecurityNumber;
        this.diagnosis = diagnosis;
        this.generalPractitioner = generalPractitioner;
    }

    /**
     * Constructor used when adding a new patient to the registry
     */
    public Patient() {
        this.firstName = null;
        this.lastName = null;
        this.socialSecurityNumber = null;
        this.diagnosis = null;
        this.generalPractitioner = null;
    }

    // Get methods for all patient attributes
    public String getFirstName() {
        return firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }
    public String getDiagnosis() {
        return diagnosis;
    }
    public String getGeneralPractitioner() {
        return generalPractitioner;
    }

    // set methods for all patients attributes
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public void setSocialSecurityNumber(String socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }
    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }
    public void setGeneralPractitioner(String generalPractitioner) {
        this.generalPractitioner = generalPractitioner;
    }

    /**
     * Equals method for comparing patients
     * @param o the object to be compared
     * @return true if objects are equal, false if not
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Patient patient = (Patient) o;
        return socialSecurityNumber == patient.socialSecurityNumber &&
                Objects.equals(firstName, patient.firstName) &&
                Objects.equals(lastName, patient.lastName) &&
                Objects.equals(diagnosis, patient.diagnosis) &&
                Objects.equals(generalPractitioner, patient.generalPractitioner);
    }
}
