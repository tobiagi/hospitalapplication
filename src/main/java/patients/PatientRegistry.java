package patients;

import java.io.*;
import java.util.ArrayList;

public class PatientRegistry {
    final static String RELATIVE_PATH = "Patients.csv";
    private ArrayList<Patient> patients;

    /**
     * Constructor for PatientRegistry
     */
    public PatientRegistry() {
        this.patients = new ArrayList<>();
    }

    /**
     * get method for all patients
     * @return arraylist of all patients in registry
     */
    public ArrayList<Patient> getPatients() {
        return this.patients;
    }

    /**
     * Method to add a patient to the registry
     * @param patient to be added to the registry
     * @return a boolean value used for testing
     */
    public boolean addPatient(Patient patient) {
        if (!this.patients.contains(patient)) {
            this.patients.add(patient);
            return true;
        } else return false;
    }

    /**
     * Method to remove a patient from the registry
     * @param patient the patient to be removed from the registry
     * @return a boolean value used for testing
     */
    public boolean removePatient(Patient patient) {
        if (this.patients.contains(patient)) {
            this.patients.remove(patient);
            return true;
        } else return false;
    }

    public boolean loadFromFile(File file) {
        try {
            FileInputStream fileInputStream = new FileInputStream(file);
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            patients = (ArrayList<Patient>) objectInputStream.readObject();
            objectInputStream.close();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
