import java.io.IOException;

import graphics.controllers.ApplicationMainController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class HospitalApplication extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    /**
     * Method that starts the program and sets scene and stage
     * @param primaryStage the stage for the main scene
     * @throws IOException throws error if error occurs
     */
    @Override
    public void start(Stage primaryStage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("applicationMain.fxml"));
        try {
            fxmlLoader.setController(new ApplicationMainController());
            Parent root = fxmlLoader.load();
            Scene scene = new Scene(root);

            primaryStage.setTitle("Hospital application");
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();

            throw e;
        }
    }
}
