package graphics.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import patients.PatientRegistry;
import patients.Patient;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

public class ApplicationMainController implements Initializable {
    private PatientRegistry patientRegistry = new PatientRegistry();
    private ObservableList<Patient> patientObservableList = FXCollections.observableArrayList();

    @FXML
    private Menu fileMenuBar;
    @FXML
    private Menu editMenuBar;
    @FXML
    private Menu helpMenuBar;
    @FXML
    private TableView<Patient> patientsTableView;
    @FXML
    private TableColumn<Patient, String> firstNameColumn;
    @FXML
    private TableColumn<Patient, String> lastNameColumn;
    @FXML
    private TableColumn<Patient, Long> socialSecurityNumberColumn;
    @FXML
    private Button addPatient;
    @FXML
    private Button removePatient;
    @FXML
    private Button editPatient;

    /**
     * Method that initializes the application's main window
     * @param url
     * @param resourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        firstNameColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        socialSecurityNumberColumn.setCellValueFactory(new PropertyValueFactory<>("socialSecurityNumber"));

        updateObservableList();
        patientsTableView.setItems(patientObservableList);
    }

    /**
     * Method for updating the table on the main window
     */
    public void updateObservableList() {
        patientObservableList.setAll(patientRegistry.getPatients());
    }

    /**
     * Method for what happens when the add patients button is pressed
     * @param actionEvent the event that happens when the button is pressed
     * @throws IOException throws error if error occurs
     */
    public void onAddPatient(ActionEvent actionEvent) throws IOException {
        PatientInfoController patientInfoController = new PatientInfoController(patientRegistry, this);
        FXMLLoader fxmlLoader = new FXMLLoader(patientInfoController.getClass().getResource("/patientInfo.fxml"));
        Stage stage = new Stage();
        fxmlLoader.setController(patientInfoController);
        stage.setTitle("Patient");
        Scene scene = new Scene(fxmlLoader.load());
        stage.setScene(scene);
        stage.show();
    }

    /**
     * Method for what happens when remove patient button is pressed
     * @param actionEvent the event that happens when the button is pressed
     */
    public void onRemovePatient(ActionEvent actionEvent) {
        Patient patient = patientsTableView.getSelectionModel().getSelectedItem();
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Delete");
        alert.setHeaderText("This patient will be deleted");
        alert.setContentText("Are you sure you want to delete this patient? (deleted patients can not be restored)");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK){
            patientRegistry.removePatient(patient);
        } else {
            alert.close();
        }
        updateObservableList();
    }

    /**
     * Method for what happens when the edit patient button is pressed
     * @param actionEvent the event that happens when the button is pressed
     * @throws IOException throws error if error occurs
     */
    public void onEditPatient(ActionEvent actionEvent) throws IOException {
        Patient patient = patientsTableView.getSelectionModel().getSelectedItem();
        PatientInfoController patientInfoController = new PatientInfoController(patient, patientRegistry, this);
        FXMLLoader fxmlLoader = new FXMLLoader(patientInfoController.getClass().getResource("/patientInfo.fxml"));
        Stage stage = new Stage();
        fxmlLoader.setController(patientInfoController);
        stage.setTitle("Patient");
        Scene scene = new Scene(fxmlLoader.load());
        stage.setScene(scene);
        stage.show();
    }

    /**
     * Method for sending an alert when help menu button is pressed
     * @param actionEvent event that occurs
     */
    public void onHelp(ActionEvent actionEvent) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information Dialog - About");
        alert.setHeaderText("Patients Register \n " +
                            "v0.1-SNAPSHOT");
        alert.setContentText("A very cool program created by \n" +
                            "(C)Tobias Giverholt \n" +
                            "2021-05-05");
        alert.showAndWait();
    }

    /**
     * Method to import data from a csv file
     * @param actionEvent
     */
    public void onImportFromCSV(ActionEvent actionEvent) {
        FileChooser fc = new FileChooser();
        FileChooser.ExtensionFilter extensionFilter = new FileChooser.ExtensionFilter("csv files", "*.csv");
        fc.getExtensionFilters().add(extensionFilter);
        File file = fc.showSaveDialog(patientsTableView.getScene().getWindow());

        if (file != null) {
            if (patientRegistry.loadFromFile(file)) {

                updateObservableList();
            }
        } else {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning");
            alert.setHeaderText("Chosen file is not valid");
            alert.setContentText("The file could not be read, please try with a different file");

            alert.showAndWait();
        }
    }
    public void onExportToCSV() {

    }
}