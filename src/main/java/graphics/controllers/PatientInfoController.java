package graphics.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import patients.Patient;
import patients.PatientRegistry;

public class PatientInfoController {
    boolean newPatient;
    Patient patient;
    PatientRegistry patientRegistry;
    ApplicationMainController applicationMainController;

    /**
     * Constructor for when registering a new patient to the registry
     * @param patientRegistry the registry that the patient will be registered to
     * @param applicationMainController the controller for the main window
     */
    public PatientInfoController(PatientRegistry patientRegistry, ApplicationMainController applicationMainController) {
        this.patientRegistry = patientRegistry;
        this.patient = new Patient();
        this.newPatient = true;
        this.applicationMainController = applicationMainController;
    }

    /**
     * Constructor for when editing a patient that already exists in the registry
     * @param patient the patient to be edited
     * @param patientRegistry the registry where the patient will be edited
     * @param applicationMainController the controller for the main window
     */
    public PatientInfoController(Patient patient, PatientRegistry patientRegistry, ApplicationMainController applicationMainController) {
        this.patientRegistry = patientRegistry;
        this.patient = patient;
        this.newPatient = false;
        this.applicationMainController = applicationMainController;
    }

    @FXML
    private TextField firstNameTextField;
    @FXML
    private TextField lastNameTextField;
    @FXML
    private TextField sscTextField;
    @FXML
    private TextField diagnosisTextField;
    @FXML
    private TextField generalPractitionerTextField;
    @FXML
    private Button okButton;
    @FXML
    private Button cancelButton;

    /**
     * Initializes the patient info window
     */
    @FXML
    void initialize() {
        if (!newPatient) {
            firstNameTextField.setText(patient.getFirstName());
            lastNameTextField.setText(patient.getLastName());
            sscTextField.setText(patient.getSocialSecurityNumber());
            diagnosisTextField.setText(patient.getDiagnosis());
            generalPractitionerTextField.setText(patient.getGeneralPractitioner());
        }
    }

    /**
     * Method for what happens when the ok button is pressed
     * @param event that occurs
     */
    public void onOkButton(ActionEvent event) {
        if (firstNameTextField.getText() == null || lastNameTextField.getText() == null || sscTextField.getText() == null || diagnosisTextField.getText() == null || generalPractitionerTextField.getText() == null) {
            Alert errorAlert = new Alert(Alert.AlertType.ERROR);
            errorAlert.setHeaderText("Input not valid");
            errorAlert.setContentText("Please fill out all information");
            errorAlert.showAndWait();
            return;
        }

        patient.setFirstName(firstNameTextField.getText());
        patient.setLastName(lastNameTextField.getText());
        patient.setSocialSecurityNumber(sscTextField.getText());
        patient.setDiagnosis(diagnosisTextField.getText());
        patient.setGeneralPractitioner(generalPractitionerTextField.getText());

        //Checks if the social security number is the right amount of characters, sends out error if not
        if (!(sscTextField.getText().length() == 11)) {
            Alert errorAlert = new Alert(Alert.AlertType.ERROR);
            errorAlert.setHeaderText("Input not valid");
            errorAlert.setContentText("Social security numbers need to be 11 characters long");
            errorAlert.showAndWait();
            return;
        }
        //Checks if patient already exists in the registry
        if (!patientRegistry.getPatients().contains(patient)) {
            patientRegistry.addPatient(patient);
        } else if (!newPatient) {
            patientRegistry.addPatient(patient);
        } else {
            Alert errorAlert = new Alert(Alert.AlertType.ERROR);
            errorAlert.setHeaderText("Patient already exists");
            errorAlert.setContentText("The patient you are trying to add already exists");
            errorAlert.showAndWait();
            return;
        }
        applicationMainController.updateObservableList();
        ((Node)(event.getSource())).getScene().getWindow().hide();
    }

    /**
     * Method for when cancel button is pressed
     * @param event that occurs
     */
    public void onCancelButton(ActionEvent event) {
        ((Node)(event.getSource())).getScene().getWindow().hide();
    }
}
