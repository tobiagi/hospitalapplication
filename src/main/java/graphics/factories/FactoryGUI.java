package graphics.factories;

import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuBar;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class FactoryGUI {

    /**
     * method returns object that is chosen
     * @param node the node that will be returned
     * @return the node
     */
    public static Node create(String node) {
        if(node.isBlank()) {
            return null;
        } else if (node.equalsIgnoreCase("tableview")) {
            return new TextField();
        } else if (node.equalsIgnoreCase("menubar")) {
            return new MenuBar();
        } else if (node.equalsIgnoreCase("vbox")) {
            return new VBox();
        } else if (node.equalsIgnoreCase("anchorpane")) {
            return new AnchorPane();
        } else if (node.equalsIgnoreCase("hbox")) {
            return new HBox();
        } else if (node.equalsIgnoreCase("button")) {
            return new Button();
        } else if (node.equalsIgnoreCase("imageview")) {
            return new ImageView();
        } else if (node.equalsIgnoreCase("gridpane")) {
            return new GridPane();
        } else if (node.equalsIgnoreCase("textfield")) {
            return new TextField();
        } else if (node.equalsIgnoreCase("label")) {
            return new Label();
        }
            return null;
    }
}
