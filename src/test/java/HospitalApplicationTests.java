import org.junit.jupiter.api.Test;
import patients.Patient;
import patients.PatientRegistry;

import static org.junit.jupiter.api.Assertions.*;

public class HospitalApplicationTests {
    private PatientRegistry patientRegistry;

    @Test
    public void setUp() {
        this.patientRegistry = new PatientRegistry();
        Patient patient = new Patient("Patient #1", "Patient #1", "11111111111", "Diagnosis #1", "Dr #1");
        Patient patient2 = new Patient("Patient #2", "Patient #2", "22222222222", "Diagnosis #2", "Dr #2");
        Patient patient3 = new Patient("Patient #3", "Patient #3", "33333333333", "Diagnosis #3", "Dr #3");

        assertTrue(patientRegistry.addPatient(patient));
        assertTrue(patientRegistry.addPatient(patient2));
        assertTrue(patientRegistry.addPatient(patient3));
    }

    @Test
    public void removePatientTest() {
        this.patientRegistry = new PatientRegistry();
        Patient patient = new Patient("Patient #1", "Patient #1", "11111111111", "Diagnosis #1", "Dr #1");
        patientRegistry.addPatient(patient);
        assertTrue(patientRegistry.removePatient(patient));
    }

    @Test
    public void duplicatePatientTest() {
        this.patientRegistry = new PatientRegistry();
        Patient patient = new Patient("Patient #1", "Patient #1", "11111111111", "Diagnosis #1", "Dr #1");
        Patient patient1 = new Patient("Patient #1", "Patient #1", "11111111111", "Diagnosis #1", "Dr #1");
        patientRegistry.addPatient(patient);
        assertFalse(patientRegistry.addPatient(patient1));
    }
}
